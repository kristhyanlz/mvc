<!DOCTYPE html>
<html lang="es">
<head>
	<link href="views/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<script src="views/js/jquery.min.js"></script> 
  <!--<script src="js/popper.min.js"></script>--> 
  <script src="views/js/bootstrap.min.js"></script>

	<title>LP3 MVC</title>
</head>
<body>
	<?php
	//require_once("controller/productos_controlador.php");
	require_once("controller/controlador.php");
	?>
</body>
</html>
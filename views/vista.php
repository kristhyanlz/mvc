<!DOCTYPE html>
<html lang="en">
<head> 
</head>
<body>

  <div class="container">
    <header class="text-center">
      <h1>Control de asistencia</h1>
    </header>
    <div class="col col-md-12 col-sm-12 col-12">
      <p>Ingrese sus datos</p>
      <p></p>
      <form class="md-form" method="post">
        <div class="form-group">
          <label for="nombre"> Nombre: </label>
          <input id="nombre" name="nombre" class="form-control" placeholder="Ingresa tu nombre" required>
          <small class="form-text text-muted">Ingresa el mismo nombre que existe en la base de datos</small>
        </div>
        <div class="form-group">
          <label for="passwd">Password</label>
          <input id="passwd" name="passwd" type="password" class="form-control" placeholder="Ingresa tu contraseña" required>
        </div>
        <div class="form-group">
          <label for="hora">Hora actual</label>
            <input id ="hora" type="time" class="form-control" value=<?php echo date("H:i", $d);?> disabled>
        </div>
        <button type="submit" class="btn btn-primary" id="boton" name="boton"> Enviar </button>
        
      </form>
      <br>
      <form method="post">
        <button type="submit" class="btn btn-primary" id="boton2" name="boton2"> Cerrar Puerta </button>
      </form>
    </div>
  </div>

  <div class="container">
    <header class="text-center">
      <h1>Historial de asistencia</h1>
    </header>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Password</th>
        <th scope="col">Hora</th>
        <th scope="col">Tipo</th>
      </th>
    </thead>
    <tbody>
      
  <?php
    foreach($trabajadores as $fila){
      echo '<tr><th scope="row">'.$fila["nombre"].'</th>';
      echo '<td>'.$fila["password"].'</td>';
      echo '<td>'.$fila["hora"].'</td>';
      echo '<td>'.$fila["tipo"].'</td>';
      echo "</tr>";
    }
  ?>
  </tbody>
  </table>
  </div>
</body>
</html>